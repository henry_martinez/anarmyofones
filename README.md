# How to build AnArmyOfOnes
This is a Gradle-based project that works best with
[Android Studio](http://developer.android.com/sdk/installing/studio.html)


To build the app:

1. Install the following software:
       - Android SDK:
         http://developer.android.com/sdk/index.html
       - Gradle:
         http://www.gradle.org/downloads
       - Android Studio:
         http://developer.android.com/sdk/installing/studio.html

2. Run the Android SDK Manager by pressing the SDK Manager toolbar button
   in Android Studio or by running the 'android' command in a terminal
   window.


3. In the Android SDK Manager, ensure that the following are installed,
   and are updated to the latest available version:
       - Tools > Android SDK Platform-tools (rev 21 or above)
       - Tools > Android SDK Tools (rev 23.0.5 or above)
       - Tools > Android SDK Build-tools version 23 (rev 23.0.1 or above)
       - Android 6.0 > SDK Platform (API 23)
       - Extras > Android Support Repository
       - Extras > Android Support Library
       - Extras > Google Play services
       - Extras > Google Repository

4. Create a file in your working directory called local.properties,
   containing the path to your Android SDK. Use local.properties.example as a
   model.

5. Import the project in Android Studio:

    1. Press File > Import Project
    2. Navigate to and choose the settings.gradle file in this project
    3. Press OK

6. Add your debug keystore to the project (save it as android/debug.keystore), 
    or modify the build.gradle file to point to your key.

7. Choose Build > Make Project in Android Studio or run the following
    command in the project root directory:
   ```
    ./gradlew clean assembleDebug
   ```
8. To install on your test device:

   ```
    ./gradlew installDebug
   ```