package com.hmartinez.an_army_of_ones;

import android.app.Application;
import android.test.ApplicationTestCase;

import com.hmartinez.an_army_of_ones.domain.CalculateExchangeInteractor;
import com.hmartinez.an_army_of_ones.model.CurrencyRates;

import junit.framework.Assert;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

}