package com.hmartinez.an_army_of_ones.domain;

import com.hmartinez.an_army_of_ones.model.CurrencyRates;

import junit.framework.Assert;
import junit.framework.TestCase;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by hmartinez on 10/18/15.
 */
public class CalculateExchangeInteractorTest extends TestCase {

    private CalculateExchangeInteractor interactor;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        HashMap<String, Double> rates = new HashMap<>();
        rates.put("GBP",0.65013);
        rates.put("JPY",119.61);
        rates.put("EUR",0.87642);
        rates.put("BRL", 3.8736);
        CurrencyRates currencyRates = new CurrencyRates("USD", rates);


        String[] array = {"GBP","JPY", "EUR", "BRL"};
        List<String> desiredCurrencies = Arrays.asList(array);
        interactor = new CalculateExchangeInteractor(currencyRates, desiredCurrencies);
    }

    public void testCurrencyShouldBeNull() throws Exception{

        final HashMap<String, Double> result = interactor.calculateExchange(4);

        final String wrongKey = "GPB";
        Assert.assertNull(result.get(wrongKey));

    }

    public void testCurrencyShouldBeCorrect() throws Exception{

        final HashMap<String, Double> result = interactor.calculateExchange(4);

        final String rightKey = "GBP";
        Assert.assertEquals(2.60052, result.get(rightKey));

    }

    public void testResultSizeShouldBeCorrect() throws Exception{

        final HashMap<String, Double> result = interactor.calculateExchange(4);

        final String rightKey = "GBP";
        Assert.assertEquals(4, result.size());

    }


}