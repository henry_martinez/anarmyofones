package com.hmartinez.an_army_of_ones.ui;

import android.support.test.InstrumentationRegistry;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.view.ViewGroup;

import com.hmartinez.an_army_of_ones.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

/**
 * Created by hmartinez on 10/18/15.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity activity;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        activity = getActivity();

    }

    public void testShowCalculatedRates() throws Exception {
        onView(withId(R.id.bills_qty_edit_text))
                .perform(typeText("123"));
        onView(withId(R.id.calculate_button))
                .perform(click());
        onView(withId(R.id.results_container))
                .check(matches(hasDescendant(withText(activity.getString(R.string.result_format, 108.27, "EUR")))));

    }

    public void testShowCalculatedRatesShouldWrong() throws Exception {
        onView(withId(R.id.bills_qty_edit_text))
                .perform(typeText("134"));
        onView(withId(R.id.calculate_button))
                .perform(click());
        final String not_expected = activity.getString(R.string.result_format, 108.27, "EUR");
        onView(withId(R.id.results_container))
                .check(matches(not(hasDescendant(withText(not_expected)))));

    }

    public void testShowCalculatedRatesShouldEmpty() throws Exception {
        onView(withId(R.id.bills_qty_edit_text))
                .perform(typeText(""));
        onView(withId(R.id.calculate_button))
                .perform(click());
        onView(withId(R.id.results_container))
                .check(matches(not(hasChilds(withId(R.id.results_container)))));

    }

    public void testShowCalculatedRatesShouldEmpty2() throws Exception {
        onView(withId(R.id.bills_qty_edit_text))
                .perform(typeText("fdd"));
        onView(withId(R.id.calculate_button))
                .perform(click());
        onView(withId(R.id.results_container))
                .check(matches(not(hasChilds(withId(R.id.results_container)))));

    }

    public static Matcher<View> hasChilds(final Matcher<View> parentMatcher) {
        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("parent does not have childs");
            }

            @Override
            public boolean matchesSafely(View view) {

                if (!(view.getParent() instanceof ViewGroup)) {
                    return parentMatcher.matches(view.getParent());
                }
                ViewGroup group = (ViewGroup) view.getParent();
                return parentMatcher.matches(view.getParent()) && (group.getChildCount() > 0);

            }
        };
    }


}