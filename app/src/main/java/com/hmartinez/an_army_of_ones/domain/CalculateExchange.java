package com.hmartinez.an_army_of_ones.domain;

import com.hmartinez.an_army_of_ones.model.CurrencyRates;

import java.util.HashMap;

/**
 *
 * Created by hmartinez on 10/16/15.
 */
public interface CalculateExchange {
    public HashMap<String, Double> calculateExchange(double billsQty);
}
