package com.hmartinez.an_army_of_ones.domain;

import com.hmartinez.an_army_of_ones.model.CurrencyRates;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hmartinez on 10/16/15.
 */
public class CalculateExchangeInteractor implements CalculateExchange{
    private CurrencyRates currencyRates;
    private List<String> currencyCodes;

    public CalculateExchangeInteractor(CurrencyRates currencyRates, List<String> desiredCurrencyCodes){
        this.currencyRates = currencyRates;
        this.currencyCodes = desiredCurrencyCodes;
    }



    @Override
    public HashMap<String, Double> calculateExchange(double billsQty) {
        HashMap<String, Double> result = new HashMap<>();
        if (currencyRates != null) {
            final int size = currencyCodes.size();
            for (int i = 0; i < size; i++) {
                String currencyCode = currencyCodes.get(i);
                if (currencyRates.getRates().containsKey(currencyCode)) {
                    final double currencyRate = currencyRates.getRates().get(currencyCode);
                    Double exchangeValue = billsQty * currencyRate;
                    result.put(currencyCode, exchangeValue);
                }
            }
        }
        return result;
    }

}
