package com.hmartinez.an_army_of_ones.domain;

import com.hmartinez.an_army_of_ones.model.CurrencyRates;

/**
 * Created by hmartinez on 10/15/15.
 */
public interface GetCurrencyRates {

    interface Callback {
        void onRatesLoaded(final CurrencyRates currencyRates);

        void onRatesNotFound();

        void onConnectionError();
    }

    void execute(final String currency, final Callback callback);
}
