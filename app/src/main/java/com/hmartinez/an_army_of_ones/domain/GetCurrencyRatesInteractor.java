package com.hmartinez.an_army_of_ones.domain;

import com.hmartinez.an_army_of_ones.model.CurrencyRates;
import com.hmartinez.an_army_of_ones.network.FixerApi;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * GeCurrencyRates Interactor
 * Created by hmartinez on 10/15/15.
 */
public class GetCurrencyRatesInteractor implements GetCurrencyRates {

    private final Retrofit retrofit;

    public GetCurrencyRatesInteractor(){
        retrofit = new Retrofit.Builder()
                .baseUrl("http://api.fixer.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    @Override
    public void execute(String currency, final Callback callback) {
        FixerApi fixerApi = retrofit.create(FixerApi.class);

        final Call<CurrencyRates> call = fixerApi.currencyRates(currency);
        call.enqueue(new retrofit.Callback<CurrencyRates>() {
            @Override
            public void onResponse(Response<CurrencyRates> response, Retrofit retrofit) {

                if (response.body() != null && response.body().getRates().size() > 0) {
                    callback.onRatesLoaded(response.body());
                } else  {
                    callback.onRatesNotFound();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onConnectionError();
            }
        });
    }


}
