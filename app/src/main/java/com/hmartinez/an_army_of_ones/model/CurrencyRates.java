package com.hmartinez.an_army_of_ones.model;

import java.util.HashMap;

/**
 * Currency rates
 * Created by hmartinez on 10/15/15.
 */
public class CurrencyRates {

    private String base;
    private HashMap<String,Double> rates;

    public CurrencyRates(String base, HashMap<String, Double> rates) {
        this.base = base;
        this.rates = rates;
    }

    public String getBase() {
        return base;
    }

    public HashMap<String, Double> getRates() {
        return rates;
    }
}
