package com.hmartinez.an_army_of_ones.network;

import com.hmartinez.an_army_of_ones.model.CurrencyRates;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by hmartinez on 10/15/15.
 */
public interface FixerApi {

    @GET("/latest")
    Call<CurrencyRates> currencyRates(@Query("base") String base);
}
