package com.hmartinez.an_army_of_ones.ui;

import java.util.HashMap;

/**
 * Created by hmartinez on 10/15/15.
 */
public interface ConverterView {

    void showProgress();

    void hideProgress();

    void showInputError();

    void showRatesError();

    void showCalculatedRates(HashMap<String, Double> exchangeResult);

    void showConnectionError();
}
