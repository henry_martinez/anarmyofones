package com.hmartinez.an_army_of_ones.ui;

import android.text.TextUtils;
import android.util.Log;

import com.hmartinez.an_army_of_ones.R;
import com.hmartinez.an_army_of_ones.domain.CalculateExchange;
import com.hmartinez.an_army_of_ones.domain.CalculateExchangeInteractor;
import com.hmartinez.an_army_of_ones.domain.GetCurrencyRates;
import com.hmartinez.an_army_of_ones.domain.GetCurrencyRatesInteractor;
import com.hmartinez.an_army_of_ones.model.CurrencyRates;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


/**
 * Currency Presenter
 * Created by hmartinez on 10/15/15.
 */
public class CurrencyPresenter implements GetCurrencyRates.Callback {

    private GetCurrencyRates getCurrencyRates;
    private CalculateExchange calculateExchange;

    private ConverterView view;
    private CurrencyRates currencyRates;
    private final List<String> desiredCurrencies;

    public CurrencyPresenter(ConverterView view) {
        this.view = view;
        this.getCurrencyRates = new GetCurrencyRatesInteractor();
        String[] array = {"GBP","JPY", "EUR", "BRL"};
        desiredCurrencies = Arrays.asList(array);
    }

    public  void initialize(){
        loadCurrencies();

    }


    public void resume(){
        if (currencyRates != null && currencyRates.getRates().size() > 0){
            loadCurrencies();
        }

    }


    public void pause(){

    }

    public void loadCurrencies(){
        view.showProgress();
        getCurrencyRates.execute("USD", this);

    }

    public void calculateExchange(String billsAmount) {
        if (!TextUtils.isEmpty(billsAmount)) {
            if (calculateExchange != null) {
                final HashMap<String, Double> exchangeResult = calculateExchange.calculateExchange(Double.parseDouble(billsAmount));
                final Double gbp = exchangeResult.get("GBP");
                Log.d("Gpb result", String.valueOf(gbp));

                view.showCalculatedRates(exchangeResult);

            } else {
                view.showRatesError();
            }

        } else {
            view.showInputError();
        }
    }

    @Override
    public void onRatesLoaded(CurrencyRates resultedCurrencyRates) {
        view.hideProgress();
        currencyRates = resultedCurrencyRates;
        calculateExchange = new CalculateExchangeInteractor(currencyRates, desiredCurrencies);
    }

    @Override
    public void onRatesNotFound() {
        view.hideProgress();
        view.showRatesError();
    }

    @Override
    public void onConnectionError() {
        view.hideProgress();
        view.showConnectionError();
    }
}
