package com.hmartinez.an_army_of_ones.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hmartinez.an_army_of_ones.R;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements ConverterView, View.OnClickListener {

    private CurrencyPresenter presenter;
    private RelativeLayout mainContainer;
    private View progressView;
    private EditText billsQtyEditText;
    private Button calculateButton;
    private LinearLayout resultsContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainContainer = (RelativeLayout) findViewById(R.id.main_container);
        progressView = findViewById(R.id.progressView);
        billsQtyEditText = (EditText)findViewById( R.id.bills_qty_edit_text );
        calculateButton = (Button)findViewById( R.id.calculate_button );
        resultsContainer = (LinearLayout)findViewById(R.id.results_container);


        calculateButton.setOnClickListener( this );

        presenter = new CurrencyPresenter(this);

        presenter.loadCurrencies();

    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.resume();
    }

    @Override
    protected void onPause() {
        presenter.pause();
        super.onPause();
    }

    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null) {
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void showProgress() {

        progressView.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgress() {

        progressView.setVisibility(View.GONE);

    }

    @Override
    public void showInputError() {
        billsQtyEditText.setError(getString(R.string.empty_input_error));
    }

    @Override
    public void showRatesError() {

        showSnackBar(getString(R.string.not_exchanges_rates_loaded));
    }

    private void showSnackBar(String message) {
        Snackbar.make(mainContainer, message, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.loadCurrencies();
                    }
                })
                .show();
    }

    @Override
    public void showCalculatedRates(final HashMap<String, Double> exchangeResult) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                resultsContainer.removeAllViews();
                for (Map.Entry<String, Double> entry : exchangeResult.entrySet()) {
                    String key = entry.getKey();
                    Double value = entry.getValue();

                    TextView result = new TextView(MainActivity.this);
                    final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.gravity = Gravity.CENTER;
                    params.setMargins(8, 8, 8, 8);
                    result.setLayoutParams(params);
                    result.setText(getString(R.string.result_format, value, key));
                    result.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
                    resultsContainer.addView(result);
                }

            }
        });
    }

    @Override
    public void showConnectionError() {
        showSnackBar(getString(R.string.connection_error));
    }

    @Override
    public void onClick(View v) {
        if (v == calculateButton) {
            presenter.calculateExchange(billsQtyEditText.getText().toString());
            hideKeyboard();
        }
    }
}
